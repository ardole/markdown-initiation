# Markdown

**Markdown** est un langage de balisage léger créé en 2004.


Peut-on affirmer que ce langage est _moderne_ ?

Voyons la [définition de moderne dans le Larousse](https://www.larousse.fr/dictionnaires/francais/moderne/51945)

> Qui appartient au temps présent ou à une époque relativement récente


Donc, puisque ce langage ~~n'appartient pas encore au passé~~, disons qu'il est moderne !

Un bon tutoriel contient toujours :
- Un bon début
- Une bonne fin
- Et si possible que le temps entre le début et la fin soit très court !

Alors finissons en là !


![](https://media.giphy.com/media/3ohzdIvnUKKjiAZTSU/giphy.gif)

|Référence|lien|
|---|---|
|Cours OpenClassrooms|https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown|
|Documentation Gitlab|https://docs.gitlab.com/ee/user/markdown.html|
